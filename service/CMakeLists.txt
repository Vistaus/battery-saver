cmake_minimum_required(VERSION 3.5)

#project(untitled3 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5 CONFIG REQUIRED Core DBus)

add_executable(batterysaver
  batterysaver.cpp
)
target_link_libraries(batterysaver Qt5::Core Qt5::DBus)
message("target dir:" ${DATA_DIR}/service ${CMAKE_CURRENT_SOURCE_DIR})
install(TARGETS batterysaver DESTINATION ${DATA_DIR}service)
install(FILES batterysaver.service DESTINATION ${DATA_DIR}service)
