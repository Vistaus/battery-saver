#ifndef BATTERYSAVER_H
#define BATTERYSAVER_H

#include <QObject>

class BatterySaver: public QObject
{
    Q_OBJECT
public:
    BatterySaver(const QString &simSlot, const QString &defaultNetwork, const QString &prefferedNetwork, bool _disableWifi, bool _disableCellularDataWifiOn, QObject *parent = 0);

private Q_SLOTS:
    void propertiesChanged(const QString &interface,
                           const QVariantMap &changed,
                           const QStringList &invalidated);
    void onNetworkingStatusChanged(const QString &interface,
                           const QVariantMap &changed,
                           const QStringList &invalidated);

private:
    void deactivateInterface(const QString& interfaceName);
    bool interfaceEnabled(const QString& interfaceName);

    QString defaultTechPrefCommand;
    QString prefTechPrefCommand;
    bool disableWifi;
    bool disableCellularDataWifiOn;
};

#endif // BATTERYSAVER_H
