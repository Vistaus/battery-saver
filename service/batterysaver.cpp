#include "batterysaver.h"

#include <QCoreApplication>
#include <QDBusConnection>
#include <QDBusArgument>
#include <QDBusReply>
#include <QDBusMetaType>
#include <QDebug>
#include <QTimer>
#include <QProcess>

#define SET_PREF_COMMAND "/usr/share/ofono/scripts/set-tech-preference %1 %2"
#define DBUS_INDICATOR_NETWORK_SERVICE "com.lomiri.indicator.network"
#define DBUS_INDICATOR_NETWORK_PATH "/com/lomiri/indicator/network"
#define DBUS_INDICATOR_NETWORK_INTERFACE "org.gtk.Actions"

BatterySaver::BatterySaver(const QString &simSlot, const QString &defaultNetwork, const QString &prefferedNetwork, bool _disableWifi, bool _disableCellularDataWifiOn, QObject *parent)
    : defaultTechPrefCommand(), prefTechPrefCommand(), disableWifi(false), QObject(parent)
{

    qDebug() <<  simSlot << defaultNetwork << prefferedNetwork << _disableWifi << _disableCellularDataWifiOn;

    defaultTechPrefCommand = QString(SET_PREF_COMMAND).arg(simSlot, defaultNetwork);
    prefTechPrefCommand = QString(SET_PREF_COMMAND).arg(simSlot, prefferedNetwork);
    disableWifi = _disableWifi;
    disableCellularDataWifiOn = _disableCellularDataWifiOn;

    qDebug() << "defaultCmd" << defaultTechPrefCommand;
    qDebug() << "prefCmd" << prefTechPrefCommand;


    bool result = QDBusConnection::sessionBus().connect(
        "com.lomiri.connectivity1",  // service
        "/com/lomiri/connectivity1/Private", // path
        "org.freedesktop.DBus.Properties",  // interface
        QStringLiteral("PropertiesChanged"), // signal
        this,
        SLOT(propertiesChanged(QString,QVariantMap,QStringList)));

    if (!result) {
        qWarning() << "error connecting dbus session";
    }

    qDebug() << "disableCellularDataWifiOn" << disableCellularDataWifiOn;
    if (disableCellularDataWifiOn) {

        result = QDBusConnection::sessionBus().connect(
            "com.lomiri.connectivity1",  // service
            "/com/lomiri/connectivity1/NetworkingStatus", // path
            "org.freedesktop.DBus.Properties",  // interface
            QStringLiteral("PropertiesChanged"), // signal
            this,
            SLOT(onNetworkingStatusChanged(QString,QVariantMap,QStringList)));

        if (!result) {
            qWarning() << "error connecting dbus NetworkingStatus";
        }
    }
}

void BatterySaver::propertiesChanged(const QString &interface,
                                     const QVariantMap &changed,
                                     const QStringList &invalidated)

{
    qDebug() << "propertiesChanged changed:" << changed;
    bool dataEnabled = QVariant(changed["MobileDataEnabled"]).toBool();
    if (dataEnabled) {
        qDebug() << "ok dataEnabled true";
        auto& cmd = prefTechPrefCommand;
        QTimer::singleShot(1000, this, [cmd, this] () {
            QProcess process;
            process.start(cmd);
            process.waitForFinished(1000);
            if (disableWifi &&  interfaceEnabled("wifi.enable")) {
                deactivateInterface("wifi.enable");
            }
        });
    } else {
        qDebug() << "ok dataEnabled false";
        auto& cmd = defaultTechPrefCommand;
        QTimer::singleShot(1000, this, [cmd] () {
            QProcess process;
            process.start(cmd);
            process.waitForFinished(1000);
        });
    }
}

void BatterySaver::onNetworkingStatusChanged(const QString &interface, const QVariantMap &changed, const QStringList &invalidated)
{
    qDebug() << "onNetworkingStatusChanged changed" << changed;


    bool wifiEnabled = QVariant(changed["WifiEnabled"]).toBool();
    qDebug() << "wifiEnabled" << wifiEnabled;
    if (!wifiEnabled) {
        return;
    }
    bool hotspotSwitchEnabled = interfaceEnabled("hotspot.enable");
    qDebug() << "HotspotEnabled" << hotspotSwitchEnabled;
    if (hotspotSwitchEnabled) {
        return;
    }

    bool cellularDataEnabled = interfaceEnabled("mobiledata.enabled");
    qDebug() << "cellularDataEnabled" << cellularDataEnabled;
    if (cellularDataEnabled) {
        deactivateInterface("mobiledata.enabled");
    }
}

void BatterySaver::deactivateInterface(const QString& interfaceName)
{

    qDebug() << "deactivateInterface" << interfaceName;
    QDBusMessage msg = QDBusMessage::createMethodCall(DBUS_INDICATOR_NETWORK_SERVICE, DBUS_INDICATOR_NETWORK_PATH, DBUS_INDICATOR_NETWORK_INTERFACE, QStringLiteral("Activate"));
    msg << interfaceName << QVariantList() << QVariantMap();

    QDBusPendingReply<void> reply = QDBusConnection::sessionBus().call(msg);

    if (!reply.isValid())
    {
        qWarning() << "Error activating interface" << interfaceName << reply.error().message();
    } else {
        qDebug() << "Interface Off";
    }
}

bool BatterySaver::interfaceEnabled(const QString& interfaceName)
{
    bool interfaceOn = false;

    QDBusMessage msg = QDBusMessage::createMethodCall(DBUS_INDICATOR_NETWORK_SERVICE, DBUS_INDICATOR_NETWORK_PATH, DBUS_INDICATOR_NETWORK_INTERFACE, QStringLiteral("Describe"));
    msg << interfaceName;
    QDBusMessage reply = QDBusConnection::sessionBus().call(msg);
    if (reply.type() == QDBusMessage::ErrorMessage)
    {
        qWarning() << "Error while checking interface state" << reply.errorMessage();
    } else {

        bool enabled;
        QDBusSignature signature;
        QVariantList values;

        const QDBusArgument &arg = reply.arguments().at(0).value<QDBusArgument>();
        if (arg.currentType() == QDBusArgument::StructureType)
        {
            arg.beginStructure();
            arg >> enabled;
            arg >> signature;
            arg >> values;
            arg.endStructure();

            interfaceOn = values.at(0).toBool();
            qDebug() << "enabled? " << interfaceOn;
        } else {
            qDebug() << "no idea what to do with " << arg.currentType();
        }

    }
    qDebug() << "enabled " << interfaceName << ":" << interfaceOn;
    return interfaceOn;
}


int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    if (!QDBusConnection::sessionBus().isConnected()) {
        qWarning("Cannot connect to the D-Bus session bus.\n"
                 "Please check your system settings and try again.\n");
        return 1;
    }

    if (argc < 6) {
        qWarning("Not enough arguments.\n");
        return 1;
    }

    qDebug() << argc << "argv" << argv[1]<< argv[2]<< argv[3]<< argv[4] << argv[5];
    BatterySaver bs(argv[1], argv[2], argv[3], QString(argv[4]) == "true" ? true : false, QString(argv[5]) == "true" ? true : false);

    return app.exec();
}
